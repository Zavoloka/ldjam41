﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BallSpawnerScript : MonoBehaviour
{

    public Transform ChildSpawnPoint = null;

    private Vector3 _spawnPoint = new Vector3(0, 0, 0);



    // Use this for initialization
    void Start()
    {

        if (ChildSpawnPoint != null)
        {
            _spawnPoint = ChildSpawnPoint.position;
        }


    }

    // Update is called once per frame
    void Update()
    {
    
    }

    public void SpawnBall()
    {
        var enemyInstance = Instantiate(Resources.Load("Prefabs/PrefabSphere/PongSphere", typeof(GameObject)), _spawnPoint, new Quaternion()) as GameObject;
        //BulletController bc = bulletInstance.GetComponent<BulletController>();
    }


    protected virtual void OnCollisionEnter(Collision collision)
    {
        GameObject collisionGameObject = collision.gameObject;
        int otherLayer = collisionGameObject.layer;

        OnInteractionHandler(ref collisionGameObject, ref otherLayer);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        GameObject collisionGameObject = other.gameObject;
        int otherLayer = collisionGameObject.layer;

        OnInteractionHandler(ref collisionGameObject, ref otherLayer);
    }


    protected virtual void OnInteractionHandler(ref GameObject collisionGameObject, ref int otherLayer)
    {
        CollisionCheck(ref collisionGameObject);
    }


    private void CollisionCheck(ref GameObject other)
    {

        if (other.CompareTag("Player"))
        {
            SpawnBall();

        }


    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySteering : MonoBehaviour {

    public int Hitpoints = 1;

    private Transform _playerTransform;
    private NavMeshAgent _navAgent;


    public Material HurtMaterial;
    public Material CurrentMaterial;
    private Renderer _currentRendrer;

    public GameObject HurtObject;


    protected AudioSource _audioSourceComponent;


    // Use this for initialization
    void Start () {

        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        _navAgent = GetComponent<NavMeshAgent>();
        _navAgent.enabled = true;

 
        _currentRendrer = gameObject.GetComponent<Renderer>();

        _audioSourceComponent = gameObject.GetComponent<AudioSource>();




    }
	
	// Update is called once per frame
	void Update () {

       // HitpointsChecker();


        if (_playerTransform == null)
        {
            return; 
        }

        if (_navAgent == null)
        {
            return;
        
        }

        if (_navAgent.enabled)
        {
            _navAgent.SetDestination(_playerTransform.position);
        }

	}


    private void HitpointsChecker()
    {
        if (Hitpoints <= 0)
        {
            _currentRendrer.material = HurtMaterial;
            _currentRendrer.enabled = false;
           Destroy(gameObject, 0.5f);
        }
        return;
    }


    public void ReceiveDamage(int amountOfDamage)
    {
        Hitpoints -= amountOfDamage;
        //Debug.Log("Hitpoints : " + this.Hitpoints);
        if (_audioSourceComponent != null)
        {
            _audioSourceComponent.Play();
        }

        if (HurtObject != null)
        {
            HurtObject.active = true;
            StartCoroutine(HurtMaterialAnimation());
        }
        
        HitpointsChecker();

        return;
    }




    protected virtual void OnCollisionEnter(Collision collision)
    {
        GameObject collisionGameObject = collision.gameObject;
        int otherLayer = collisionGameObject.layer;

        OnInteractionHandler(ref collisionGameObject, ref otherLayer);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        GameObject collisionGameObject = other.gameObject;
        int otherLayer = collisionGameObject.layer;

        OnInteractionHandler(ref collisionGameObject, ref otherLayer);
    }


    protected virtual void OnInteractionHandler(ref GameObject collisionGameObject, ref int otherLayer)
    {
        CollisionCheck(ref collisionGameObject);
    }


    private void CollisionCheck(ref GameObject other)
    {

        if (other.CompareTag("Player"))
        {

            PlayerControl pObj = other.GetComponent<PlayerControl>();
            if (pObj != null)
            {
                pObj.ReceiveDamage(1);
            }
        }


    }
    protected IEnumerator HurtMaterialAnimation()
    {

        yield return new WaitForSeconds(0.3f);
        HurtObject.active = false;

    }

    /*
    protected IEnumerator HurtMaterialAnimation()
    {
        if (HurtMaterial != null)
        {

            if (CurrentMaterial == null)
            {

                StopCoroutine(HurtMaterialAnimation());

            }
            _currentRendrer.material = HurtMaterial;

            yield return new WaitForSeconds(0.3f);


            if (CurrentMaterial != null)
            {
                _currentRendrer.material = CurrentMaterial;
            }

        }
  */
}
   







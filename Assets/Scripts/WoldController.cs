﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoldController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerExit(Collider other)
    {
        // Destroy everything that leaves the trigger
        Destroy(other.gameObject);
    }

   
    void OnCollisionExit(Collision collisionInfo)
    {
        Destroy(collisionInfo.gameObject);
    }
    
}
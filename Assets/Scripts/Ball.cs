﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {


    public int DamageValue = 1;

    float ax;
    float ay;
    float az;

    Rigidbody _rb;

    protected AudioSource _audioSourceComponent;

    // Use this for initialization
    void Start () {

        _rb = GetComponent<Rigidbody>();

        _audioSourceComponent = gameObject.GetComponent<AudioSource>();
        BounceStarted();

        //Destroy(gameObject, 5);
    }


    private void BounceStarted()
    {

        ax = Random.Range(0, 2) == 0 ? -1 : 1;
        ay = Random.Range(0, 2) == 0 ? -1 : 1;
        az = Random.Range(0, 2) == 0 ? -1 : 1;

        _rb.velocity = new Vector3(Random.Range(15, 30) * ax, Random.Range(15, 20) * ay, Random.Range(15, 30) * az);
    }

	
	// Update is called once per frame
	void Update () {


	}


    protected virtual void OnCollisionEnter(Collision collision)
    {
        GameObject collisionGameObject = collision.gameObject;
        int otherLayer = collisionGameObject.layer;

        OnInteractionHandler(ref collisionGameObject, ref otherLayer);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        GameObject collisionGameObject = other.gameObject;
        int otherLayer = collisionGameObject.layer;

        OnInteractionHandler(ref collisionGameObject, ref otherLayer);
    }


    protected virtual void OnInteractionHandler(ref GameObject collisionGameObject, ref int otherLayer)
    {
        CollisionCheck(ref collisionGameObject);
    }


    private void CollisionCheck(ref GameObject other)
    {

        if (other.CompareTag("Player"))
        {
            BounceStarted();
           // Debug.Log("Player collider");
        }

        if (other.CompareTag("Enemy"))
        {

            //Debug.Log("enemy collision");
            EnemySteering esObj = other.GetComponent<EnemySteering>();
            if (esObj != null)
            {
               // Debug.Log("enemy damag");
                esObj.ReceiveDamage(DamageValue);
            }
           /* if (_audioSourceComponent != null)
            {
                _audioSourceComponent.Play();
            }*/
            BounceStarted();
           // Debug.Log("Enemy collider");
        }


        if (other.CompareTag("ScoreManager"))
        {
            other.GetComponent<ScoreCounter>().AddScore(1);
            BounceStarted();
           // Debug.Log("Score collider");
        }



    }

}

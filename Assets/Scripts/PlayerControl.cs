﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {


    public int Hitpoints = 10;
    public Text HealthText = null;
    private int _maxHitPoints = 0;

    public Material HurtMaterial;
    public Material CurrentMaterial;
    private Renderer _currentRendrer;

    protected AudioSource _audioSourceComponent;

    public GameObject HurtObject;



    // Use this for initialization
    void Start () {
        _maxHitPoints = Hitpoints;
        _currentRendrer = gameObject.GetComponent<Renderer>();
        _audioSourceComponent = gameObject.GetComponent<AudioSource>();


    }
	
	// Update is called once per frame
	void Update () {

        HitpointsChecker();


        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("LevelZero");
        }

        float xrot = 0.0f;
        float zrot = 0.0f;

        if (Input.GetKeyDown(KeyCode.Q))
        {
            zrot = 10.0f;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            zrot = 10.0f;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 50.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 30.0f;




      //  transform.Rotate(0, zrot, 0);
        transform.Translate(x, 0, z);




	}



    private void HitpointsChecker()
    {
        if (Hitpoints <= 0)
        {
            Destroy(this);
            SceneManager.LoadScene("LevelZero");
        }
        return;
    }

    public void ReceiveDamage(int amountOfDamage)
    {
        this.Hitpoints -= amountOfDamage;

        HealthText.text = "Player health " + Hitpoints.ToString() + "/" + _maxHitPoints;
        // Debug.Log("Hitpoints : " + this.Hitpoints);
        if (_audioSourceComponent != null)
        {
            _audioSourceComponent.Play();
        }

        if (HurtObject != null)
        {
            HurtObject.active = true;
            StartCoroutine(HurtMaterialAnimation());
        }

       
        return;
    }

    protected IEnumerator HurtMaterialAnimation()
    {

        yield return new WaitForSeconds(0.3f);
        HurtObject.active = false;

    }
    /*
    protected IEnumerator HurtMaterialAnimation()
    {
        if (HurtMaterial != null)
        {

            if (CurrentMaterial == null)
            {

                StopCoroutine(HurtMaterialAnimation());

            }
            _currentRendrer.material = HurtMaterial;

            yield return new WaitForSeconds(0.3f);


            if (CurrentMaterial != null)
            {
                _currentRendrer.material = CurrentMaterial;
            }

        }

    }*/
/*
    void OnDestroy()
    {
        //print("Script was destroyed");
        SceneManager.LoadScene("LevelZero");
    }*/

    /*
     * 
     * 
     *   public void DecreaseHitPoints(float val)
        {

            _characterClassComponent.HealthBar.Decrease(val);
     
            if (_audioSourceComponent != null)
            {
                _audioSourceComponent.Play();
            }
          
            StartCoroutine(HurtMaterialAnimation());
            if (isDead())
            {
                Destroy();
            }
                  
        }


        protected IEnumerator HurtMaterialAnimation()
        {
            if (HurtMaterial != null)
            {
               
                if (_currentRendrer == null)
                {

                    StopCoroutine(HurtMaterialAnimation());
                    
                }
                _currentRendrer.material = HurtMaterial;

                yield return new WaitForSeconds(HurtSeconds);


                if (CurrentMaterial != null)
                {
                    _currentRendrer.material = CurrentMaterial;
                }
    
             }

        }
     * */

}

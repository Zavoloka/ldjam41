﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour {

    public int MaxScore = 5;
    public int CurrentScore = 0;
    public Transform ChildSpawnPoint = null;

    public Text ScoreText = null;

    protected AudioSource _audioSourceComponent;

    public Text TimerHUD;
    float Timer = 0.0f;

    public Text Grats;
    public AudioSource GratsSound;

    private Vector3 _spawnPoint = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {

        if (ChildSpawnPoint != null)
        {
            _spawnPoint = ChildSpawnPoint.position;
        }

        _audioSourceComponent = gameObject.GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {

        Timer += Time.deltaTime;

 
        TimerHUD.text = (Math.Round(Timer, 2)).ToString();

        //if (CurrentScore >= MaxScore)
       // {
            // CONGRATULATION!!!!
            // SHOW WIN MESSAGE

          //  SceneManager.LoadScene("LevelZero");

       // } 
		
	}

    public void SpawnEnemy()
    {
        var enemyInstance = Instantiate(Resources.Load("Prefabs/EnemyPrefab/Enemy", typeof(GameObject)), _spawnPoint, new Quaternion()) as GameObject;
        //BulletController bc = bulletInstance.GetComponent<BulletController>();
    }

    public void AddScore(int scoreValue)
    {

 
        CurrentScore += scoreValue;
        ScoreText.text = "Score " + CurrentScore.ToString() +"/" + MaxScore.ToString();
        if (_audioSourceComponent != null)
        {
            _audioSourceComponent.Play();
        }
       
        if (ScoreChecker())
        {
            SpawnEnemy();
        }

        //Debug.Log("current log : " + CurrentScore);
    
    }

    public bool ScoreChecker()
    {
        if (CurrentScore >= MaxScore)
        {

            Grats.gameObject.SetActive(true);
            GratsSound.Play();
            return false;
        }

        return true;

    }
}
